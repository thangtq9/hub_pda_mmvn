﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System.Data;
using System.Net.Http.Headers;
using Web.Api.Helps;
using Web.Api.Model;

namespace Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class TemplateAuthenController : ControllerBaseToken
    {
        private readonly IWebHostEnvironment _environment;
        public TemplateAuthenController(IWebHostEnvironment environment)
        {
            _environment = environment;
        }
        [HttpPost("listprovince")]
        public async Task<ActionResult> listprovince()
        {
            // Tạo một mảng các đối tượng JSON
            var arrayOfObjects = new[]
            {new { id = 0, name = "--All--" },
                new { id = 1, name = "province 1" },
                new { id = 2, name = "province 2" },
                new { id = 3, name = "province 3" }
            };
            return Ok(arrayOfObjects);
        }
        [HttpPost("listdistrict")]
        public async Task<ActionResult> listdistrict(int province_id)
        {
            var arrayOfObjects = new[]
            {
                new { id = 1, name = "district 1",province_id= 1 },
                new { id = 2, name = "district 12" ,province_id= 1},
                new { id = 3, name = "district 2",province_id= 2 }
            };
            var d = (from p in arrayOfObjects where p.province_id == province_id select new { id = p.id, name = p.name }).ToList();
            return Ok(d);
        }

        [HttpPost("reportdata")]
        public async Task<ActionResult> reportdata(string fromdate, string todate, int province_id, int district_id, int towner_id, int pageindex = 1, int pagesize = 10)
        {
            var arrayOfObjects = new[]
            {
                new { id = 1, name = "towner 1",district_id= 1 ,isdetail = 0,progress = 30},
                new { id = 2, name = "towner 12" ,district_id= 1,isdetail = 0,progress = 50},
                new { id = 3, name = "towner 2",district_id= 2 ,isdetail = 0,progress = 80}
            };
            return Ok(arrayOfObjects);
        }

        [HttpPost("reportdatakpi")]
        public async Task<ActionResult> reportdatakpi(int id)
        {
            var arrayOfObjects1 = new[]
            {
                new { id = 1, name = "kpi 1"},
                new { id = 2, name = "kpi 2" },
                new { id = 3, name = "kpi 3"}
            };
            var arrayOfObjects2 = new[]
            {
                 new { id = 1, name = "kpi 1"},
                new { id = 3, name = "kpi 3"}
            };
            if (id == 1)
                return Ok(arrayOfObjects1);
            else
                return Ok(arrayOfObjects2);
        }

        [HttpPost("exporrawdata")]
        public async Task<ActionResult> exporrawdata(string fromdate, string todate, int province_id, int district_id, int towner_id, int pageindex = 1, int pagesize = 10)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(int));
            dt.Columns.Add("name", typeof(string));
            for (int i = 0; i <= 10; i++)
            {
                DataRow dr = dt.NewRow();
                dr["id"] = i;
                dr["name"] = i.ToString();
                dt.Rows.Add(dr);
            }
            string sWebRootFolder = _environment.WebRootPath;
            string name = "RawData_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("hhmmss") + ".xlsx";
            string sFileName = string.Format("Export/FileExport/{0}", name);
            string url = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo template = new FileInfo(Path.Combine(sWebRootFolder, "Export/Template/tmp_ExportRawData.xlsx"));
            string fileExport = Path.Combine(sWebRootFolder, sFileName);
            FileInfo fileCopy = template.CopyTo(fileExport);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var xls = new ExcelPackage(fileCopy))
            {
                ExcelWorksheet wsOSA = xls.Workbook.Worksheets[0];
                wsOSA.Cells[1, 1].LoadFromDataTable(dt, true);
                xls.Save();
                xls.Dispose();
            }
            //if (System.IO.File.Exists(fileExport))
            //{
            //    // Đọc nội dung của file Excel vào một mảng byte
            //    byte[] fileBytes = System.IO.File.ReadAllBytes(fileExport);

            //    // Trả về file Excel dưới dạng phản hồi tải xuống
            //    return File(fileBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name);
            //}

            return Ok(new { linkfile = url });
        }

        [HttpPost("importfile"), DisableRequestSizeLimit]
        public async Task<ActionResult> importfile(int year, int month,string Langua_FE, [FromForm] IFormFile formDataUpload)
        {
            //var file = Request.Form.Files[0];
            string error = "File Error.";
            try
            {
                var file = formDataUpload;
                string folderName = "import/excel";
                string webRootPath = _environment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                string fullPath = Path.Combine(newPath, fileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                // Read file
                FileInfo importFile = new FileInfo(fullPath);
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(importFile))
                {
                    ExcelWorksheet wsOSA = package.Workbook.Worksheets[0];
                    DataTable dt = new DataTable();
                    dt.Columns.Add("id", typeof(int));
                    dt.Columns.Add("name", typeof(string));
                }
                error = "Import thành công";
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            return Ok(new { result = error });
        }

        [HttpPost("permisions")]
        public async Task<ActionResult> permisions()
        {
            return Ok(permisions);
        }
    }
}
