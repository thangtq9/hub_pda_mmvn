﻿namespace Web.Api.Model
{
    public class MenuModel
    {
        public int menu_id { get; set; }
        public string menu_name { get; set; }
        public string menu_router { get; set; }
    }
}
