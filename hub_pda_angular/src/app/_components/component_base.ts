// permission-base.component.ts
import { Component, Inject } from '@angular/core';
import { AuthenticationService } from '../_services';

@Component({
    template: ''
})
export class ComponentBase {
    hasPermission(permissionid: number): boolean {
        // this.service.getListDistrict(this.province_id).subscribe(result => {
        //     this.districts = result;
        //     this.districts.splice(0, 0, ob_all);
        //     this._district_id = this.districts[0].id;
        //     this.changeDistrict();
        //   });    
        return true;
    }

    get currentUser() {
        var u = localStorage.getItem('currentUser');
        if (u === '' || u === null) {
            return null;
        }
        else
            return JSON.parse(u);
    }

}