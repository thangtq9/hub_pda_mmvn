import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { CommonService } from 'src/app/_services/common.service';

@Component({
  selector: 'app-control-district',
  templateUrl: './control-district.component.html',
  styleUrls: ['./control-district.component.css']
})
export class ControlDistrictComponent implements OnInit {
  districts: any;
  _district_id: any;
  @Input() province_id: number = 0;
  @Output() returnDistrictId = new EventEmitter();

  constructor(private service: CommonService) { }

  ngOnInit() { }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges) {
    if (changes['province_id']) {
      this._district_id = 0;
      let ob_all = {
        id: 0,
        name: "--All--"
      }
      this.service.getListDistrict(this.province_id).subscribe(result => {
        this.districts = result;
        this.districts.splice(0, 0, ob_all);
        this._district_id = this.districts[0].id;
        this.changeDistrict();
      });
    }
  }

  changeDistrict() {
    this.returnDistrictId.emit(this._district_id);
  }

}
