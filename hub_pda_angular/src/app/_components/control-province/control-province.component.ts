import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { CommonService } from 'src/app/_services/common.service';

@Component({
  selector: 'app-control-province',
  templateUrl: './control-province.component.html',
  styleUrls: ['./control-province.component.css']
})
export class ControlProvinceComponent implements OnInit {
  provinces: any;
  _province_id : any = 0;
  @Output() returnValue = new EventEmitter();

  constructor(private service: CommonService) {}

  ngOnInit() {
      this._province_id = 0;
      this.service.getListProvince().subscribe(result => {
        this.provinces = result;
        this._province_id = this.provinces[0].id;
        this.changeProvince();
      });
  }

  changeProvince() {
    this.returnValue.emit(this._province_id);
  }
}
