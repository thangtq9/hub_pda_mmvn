import { ControlDistrictComponent } from "./control-district/control-district.component";
import { ControlProvinceComponent } from "./control-province/control-province.component";

export let CUSTOM_COMPONENTS = [ControlProvinceComponent,ControlDistrictComponent];