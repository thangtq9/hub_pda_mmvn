import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core'

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private translate: TranslateService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        var _currentUser = localStorage.getItem('currentUser');

        // Lấy ngôn ngữ hiện tại từ TranslateService
        const currentLanguage = this.translate.currentLang;
        if (_currentUser != null) {
            let currentUser = JSON.parse(_currentUser);
            if (currentUser && currentUser.token) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${currentUser.token}`,
                        ContentType: 'application/json',
                        //Language_FE : currentLanguage
                    },
                    setParams:{
                        "Langua_FE":currentLanguage
                    }
                });
            }
        }

        return next.handle(request);
    }


}