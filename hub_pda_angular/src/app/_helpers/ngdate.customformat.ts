import { NgbDateParserFormatter,NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';

function padNumber(value: number) {
  if (!isNaN(value) && value !== null) {
    return `0${value}`.slice(-2);
  } 
  return '';
}

@Injectable()
export class NgbDateCustomParserFormatter extends NgbDateParserFormatter {
  override parse(value: string): NgbDateStruct | null {
      throw new Error('Method not implemented.');
  }
    // parse(value: string): NgbDateStruct {
    //     if (value) {
    //         const dateParts = value.trim().split('/');

    //         let dateObj: NgbDateStruct = { day: <any>null, month: <any>null, year: <any>null }
    //         const dateLabels = Object.keys(dateObj);
      
    //         dateParts.forEach((datePart, idx) => {
    //           dateObj[dateLabels[idx]] = parseInt(datePart, 10) || <any>null;
    //         });
    //         return dateObj;
    //     }
    //     return {day: 0, month: 0, year: 0};
    // }
 
  static formatDate(date: NgbDateStruct | NgbDate | null): string {
    return date ?
        `${padNumber(date.day)}/${padNumber(date.month)}/${date.year || ''}` :
        '';
  }
 
  format(date: NgbDateStruct | null): string {
    return NgbDateCustomParserFormatter.formatDate(date);
  }
}