import { Component, OnInit } from '@angular/core';

import {AuthenticationService} from 'src/app/_services';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.css']
})
export class AppLayoutComponent implements OnInit {

  user_info : any;
  constructor(
    private authen : AuthenticationService,
    private transervice:TranslateService
  ) { 
    transervice.setDefaultLang("en");
    transervice.use('en');
  }

  ngOnInit() {
    this.user_info = this.authen.currentUser;
  }
  switch_lg(lang:string) {
    this.transervice.use(lang);
   }

}
