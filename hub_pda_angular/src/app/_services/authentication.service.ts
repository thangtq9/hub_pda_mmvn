import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ConfigService } from './ConfigService';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {

        var options = {
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST'
            },
            method: 'POST',
            mode: 'no-cors'
        };

        return this.http.post<any>(`${ConfigService.apiUrl}/api/users/authenticate`, { username, password }, options)
            .pipe(map(user => {

                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));
    }

    getRouterUrl(RouterUrl: any) {
        return this.http.post(`${ConfigService.apiUrl}/api/employees/RouterUrl`, { RouterUrl });
    }

    get currentUser() {
        var u = localStorage.getItem('currentUser');
        if (u === '' || u === null) {
            return null;
        }
        else
            return JSON.parse(u);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    getPermistion() {
        return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/permisions', null);
    }
}