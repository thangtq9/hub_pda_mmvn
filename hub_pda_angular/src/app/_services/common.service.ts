import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '.';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(private http: HttpClient) {}
  

  getListProvince() {
    return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/listprovince',null);
  }

  getListDistrict(province_id : number = 0) {
    return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/listdistrict?province_id='+ province_id,null);
  }
  getListTown(district_id : number = 0) {
    return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/listtowner?district_id='+ district_id,null);
  }
}
