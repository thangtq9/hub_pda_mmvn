export * from './ConfigService';
export * from './alert.service';
export * from './authentication.service';
export * from './user.service';
export * from './common.service';
export * from './report.service';
export * from './translate.component.service';


import { AlertService } from './alert.service';
import { AuthenticationService } from './authentication.service';    
import { CommonService } from './common.service';
import { ReportService } from './report.service';
import { UserService } from './user.service'; 
import { TranslateComponentService } from './translate.component.service'; 
      
export let MODULE_SERVICE = [AlertService, AuthenticationService, UserService,CommonService,ReportService,TranslateComponentService];