import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '.';

@Injectable({
  providedIn: 'root'
})
export class  ReportService {
  constructor(private http: HttpClient) {}
  
  getReportData(fromdate: string, todate:string,province_id:number,district_id:number,towner_id:number,pageindex:number  = 1, pagesize:number = 10) {
    return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/reportdata?fromdate='
    +fromdate+'&todate='+todate+'&province_id='+province_id+'&district_id='+district_id +'&towner_id='+towner_id +'&pageindex=' +pageindex +'&pagesize=' +pagesize,null);
  }

  getReportDataDetail(id: number) {
    return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/reportdatakpi?id='
    +id,null);
  }


  exportrawdata(fromdate: string, todate:string,province_id:number,district_id:number,towner_id:number,pageindex:number  = 1, pagesize:number = 10) {
    return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/exporrawdata?fromdate='
    +fromdate+'&todate='+todate+'&province_id='+province_id+'&district_id='+district_id +'&towner_id='+towner_id +'&pageindex=' +pageindex +'&pagesize=' +pagesize,null);
  }
  importrawdata(year: number, month:number,formDataUpload: FormData) {
    return this.http.post<any>(ConfigService.apiUrl + '/api/templateauthen/importfile?year='+year+'&month='+month,formDataUpload);
  }
}
