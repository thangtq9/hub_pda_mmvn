import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TranslateComponentService {
    constructor(
        private translate: TranslateService
    ) {
    }

    getLabel(key: string): string {
        var mess_export_success = "";
        this.translate.get("label." + key).subscribe((res: string) => {
            mess_export_success = res;
        });
        return mess_export_success;
    }
    getMessage(key: string): string {
        var mess_export_success = "";
        this.translate.get("message."+key).subscribe((res: string) => {
            mess_export_success = res;
        });
        return mess_export_success;
    }
}