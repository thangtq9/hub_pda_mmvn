import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './ConfigService';


@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<any>(`${ConfigService.apiUrl}/api/users`);
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id);
    }

    create(user: any) {
        return this.http.post('/api/users', user);
    }

    update(user: any) {
        return this.http.put('/api/users/' + user.id, user);
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id);
    }
}