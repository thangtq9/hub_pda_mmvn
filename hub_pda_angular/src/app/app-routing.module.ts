import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards';
import { AppLayoutComponent } from './_layout/app-layout/app-layout.component';
import { ManagerDataComponent } from './tools';
import { ReportDataComponent } from './reports/index';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    canActivate: [AuthGuard],
    children: [
        //{ path: '', component: DashboardComponent, pathMatch: 'full' },
        { path: 'reports/reportdata', component: ReportDataComponent, canActivate: [AuthGuard] },
        { path: 'tools/managerdata', component: ManagerDataComponent, canActivate: [AuthGuard] },
        //{ path: 'employees/employee-list', component: EmployeeListComponent, canActivate: [AuthGuard] },
        //{ path: 'employees/detail/:id', component: EmployeeDetailComponent, canActivate: [AuthGuard] },

    ]
},
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
