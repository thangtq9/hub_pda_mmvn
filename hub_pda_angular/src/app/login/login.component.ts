import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService,AlertService } from '../_services/index';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;
  submitted = false;
  returnUrl = "";
  username_test: string = '';
  password_test: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private toasrtService: ToastrService,
    private router: Router,) { }


  ngOnInit() {
     // reset login status
     this.authenticationService.logout();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  login() {
    this.loading = true;
    this.authenticationService.login(this.username_test, this.password_test)
      .subscribe(
        data => {
          this.toasrtService.success("Thành công","Thông báo");
          this.loading = false;
          //location.href='reports/reportdata';
          this.router.navigateByUrl('reports/reportdata');  
          // if (data.employeeType === 9 || data.employeeType === 10) {
          //   this.router.navigateByUrl('/qc/results');
          // }
          // else if (data.employeeType === 3) {
          //   this.router.navigateByUrl('');
          // }
          // else if (data.employeeType === 1) {
          //   this.router.navigateByUrl('/reports/work-results');
          //   localStorage.setItem('menu_selected', "19");
          // }
          // else if (data.employeeType === 19
          //   || data.employeeType === 20
          //   || data.employeeType === 21
          //   || data.employeeType === 22
          //   || data.employeeType === 23
          //   || data.employeeType === 24) {
          //   this.router.navigateByUrl('/reports/work-results');
          //   localStorage.setItem('menu_selected', "35");
          // }
          // else {
          //   //this.router.navigate([this.returnUrl]); 
          //   this.router.navigateByUrl('/reports/customer-gt');
          //   localStorage.setItem('menu_selected', "35");
          // }
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
