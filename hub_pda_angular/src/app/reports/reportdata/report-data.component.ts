import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbDate, NgbCalendar, NgbModal,ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService, ReportService,TranslateComponentService } from 'src/app/_services';
import { ComponentBase } from 'src/app/_components/component_base';

@Component({
  selector: 'app-report-data',
  templateUrl: './report-data.component.html',
  styleUrls: ['./report-data.component.css']
})
export class ReportDataComponent  implements OnInit {
  fromdate: NgbDate;
  page: number = 1;
  pageSize: number = 20;
  totalRows: number = 0;
  todate: NgbDate;
  province_id: any;
  district_id: any;

  reportdata: any;
  linkdownload: string = '';

  constructor(
    private toastr: ToastrService,
    private reportService: ReportService,
    private calendar: NgbCalendar,
    private translate_com: TranslateComponentService,
    private modalService: NgbModal,
    service: AuthenticationService
  ) {
    const today = calendar.getToday(); 
    this.fromdate = calendar.getNext(today, 'd',-1); //calendar.getToday();
    this.todate = calendar.getNext(today, 'm', 1);// calendar.getNext(today, 'd',7);// calendar.getToday();
  }

  selectedCar: number = 0;
  selectedCity: any;
  selectedCityIds: any;
  selectedCityObj: any;
  cars = [
    { id: 0, name: '--All--' },
    { id: 1, name: 'Volvo' },
    { id: 2, name: 'Saab' },
    { id: 3, name: 'Opel' },
    { id: 4, name: 'Audi' },
  ];


  formatDate(date: NgbDate): string {
    return `${date.year}-${date.month < 10 ? '0' + date.month : date.month}-${date.day < 10 ? '0' + date.day : date.day}`;
  }

  ngOnInit() {

  }
  openDatePicker() {
    // Do something when the datepicker is opened
  }
  filter() {
    try {
      //this.toastr.error(this.formatDate(this.fromdate));
      // Lấy thời gian hiện tại
      // const currentDateTime = new Date();
      // // Định dạng thời gian hiện tại thành chuỗi (dd/MM/yyyy HH:mm:ss)
      // const formattedDateTime = `${currentDateTime.getDate()}/${currentDateTime.getMonth() + 1}/${currentDateTime.getFullYear()} ${currentDateTime.getHours()}:${currentDateTime.getMinutes()}:${currentDateTime.getSeconds()} ${currentDateTime.getMilliseconds()}`;

      // // In thời gian hiện tại vào console.log
      // console.log(`Current Date and Time: ${formattedDateTime}`);

      this.reportService.getReportData(this.formatDate(this.fromdate), this.formatDate(this.todate), this.province_id, this.district_id, 0, 1, 20).subscribe(result => {
        this.reportdata = result;
      });

    } catch (error) {
      this.toastr.error(error + '');
    }
  }
  viewdetail(item_result: any) {
    if (item_result.isdetail == 0) {
      this.reportService.getReportDataDetail(item_result.id).subscribe(result => {
        item_result.isdetail = 1;
        item_result.tabselect = "1";
        item_result.kpilist= result;
        console.log(JSON.stringify(result));

        result.forEach((element: any) => {
          if(element.id == 1)
          {item_result.kpi_1 = 1;}

          if(element.id == 2)
          {item_result.kpi_2 = 1;}

          if(element.id == 3)
          {item_result.kpi_3 = 1;}
            
        });

      });
    }
    else
    {
      item_result.isdetail = 0;
    }
  }

  export() {
    try {
      this.reportService.exportrawdata(this.formatDate(this.fromdate), this.formatDate(this.todate), this.province_id, this.district_id, 0, 1, 20).subscribe(result => {
        this.toastr.success(this.translate_com.getMessage("export_sucess"), "Thông báo");
        this.linkdownload = result.linkfile;
      });
    } catch (error) {
      this.toastr.error(error + '');
    }
  }

  getProvince($event: any) {
    this.province_id = $event;
  }
  getDistrict($event: any) {
    this.district_id = $event;
  }

  closeResult:any;
  open(content : any) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

  modeldetail(content : any,workid : number) {
		this.modalService.open(content, {size: 'xl', ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

  modelclose(modal : any) {
		modal.dismiss('Cross click');
    console.log('event close');
	}

  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}
}
