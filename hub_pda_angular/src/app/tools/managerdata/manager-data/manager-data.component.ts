import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ComponentBase } from 'src/app/_components/component_base';
import { ReportService, ConfigService } from 'src/app/_services';

@Component({
  selector: 'app-manager-data',
  templateUrl: './manager-data.component.html',
  styleUrls: ['./manager-data.component.css']
})
export class ManagerDataComponent extends ComponentBase implements OnInit {
  active_tab: string = "1";
  tab_3: number = 0;
  link_template: string = '';

  constructor(private reportService: ReportService,
    private toastService: ToastrService
    ) {
      super();
  }
  ngOnInit(): void {
    this.link_template = ConfigService.apiUrl + '/Export/Template/tmp_ExportRawData.xlsx';
  }

  importfile(files: any) {
    if (files.length === 0) {
      return;
    }
    const formDataUpload = new FormData();

    formDataUpload.append('formDataUpload', files[0]);
    console.log(formDataUpload);
    this.reportService.importrawdata(2023, 9, formDataUpload).subscribe(result => {
      this.toastService.success(result.result);
    });

  }


}
